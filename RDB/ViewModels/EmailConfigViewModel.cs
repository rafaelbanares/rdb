﻿namespace RDB.ViewModels
{
    public class EmailConfigViewModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
