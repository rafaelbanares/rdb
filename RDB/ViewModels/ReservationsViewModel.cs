﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RDB.ViewModels
{
    public class ReservationsViewModel
    {
        public ReservationsViewModel()
        {
            Companions = new List<ReservationCompanionViewModel>();
        }

        public string CustomErrorMessage { get; set; }

        [Required]
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EventDate { get; set; }

        [Required]
        [DisplayName("Schedule")]
        public int? ScheduleId { get; set; }

        [Required]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [Range(15, 65, ErrorMessage = "Must be between 15-65 years old.")]
        public short? Age { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DisplayName("Contact #")]
        public string ContactNo { get; set; }

        [DisplayName("Email Address")]
        [EmailAddress]
        public string Email { get; set; }

        public IList<ReservationCompanionViewModel> Companions { get; set; }
    }

    public class ReservationCompanionViewModel
    {
        public string CompanionName { get; set; }

        [Range(15, 65, ErrorMessage = "Must be between 15-65 years old.")]
        public short? Age { get; set; }
    }

    public class ReservationSuccessViewModel
    {
        public string SuccessMessage { get; set; }
        public string QRCode { get; set; }
    }

    public class ReservationListViewModel
    {
        public ReservationListViewModel()
        {
            //ReservationTable = new List<Tuple<string, int>>();
            ReservationTable = new List<ReservationRowViewModel>();
        }

        [Required]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EventDate { get; set; }

        public int ScheduleId { get; set; }

        public string ScheduleDescription { get; set; }

        public IList<ReservationRowViewModel> ReservationTable { get; set; }

        //public IList<Tuple<string, int>> ReservationTable { get; set; }
    }

    public class ReservationRowViewModel
    {
        public string FullName { get; set; }

        public IList<string> CompanionNames { get; set; }

        public string SeatNumbers { get; set; }
    }

}
