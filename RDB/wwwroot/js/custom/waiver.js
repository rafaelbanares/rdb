﻿jQuery(document).ready(function () {
    $('#chkWaiver').change(function () {
        if ($(this).is(":checked")) {
            $('button[type="submit"]').prop('disabled', false);
        }
        else {
            $('button[type="submit"]').prop('disabled', true);
        }
    });

});