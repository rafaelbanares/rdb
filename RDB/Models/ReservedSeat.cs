﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RDB.Models
{
    public partial class ReservedSeat
    {
        public Guid ReservationId { get; set; }
        public short SeatNo { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
