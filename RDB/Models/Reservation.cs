﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RDB.Models
{
    public partial class Reservation
    {
        public Reservation()
        {
            Companions = new HashSet<Companion>();
            ReservedSeats = new HashSet<ReservedSeat>();
        }

        public Guid Id { get; set; }
        public DateTime EventDate { get; set; }
        public int ScheduleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string EmailAddr { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual Schedule Schedule { get; set; }
        public virtual ICollection<Companion> Companions { get; set; }
        public virtual ICollection<ReservedSeat> ReservedSeats { get; set; }
    }
}
