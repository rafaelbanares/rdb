﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RDB.Models
{
    public partial class Companion
    {
        public int Id { get; set; }
        public Guid ReservationId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
