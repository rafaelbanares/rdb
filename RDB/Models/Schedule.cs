﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RDB.Models
{
    public partial class Schedule
    {
        public Schedule()
        {
            Reservations = new HashSet<Reservation>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public int Capacity { get; set; }

        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}
