﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QRCoder;
using RDB.Models;
using RDB.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Options;
using RDB.Helpers;

namespace RDB.Controllers
{
    public class HomeController : Controller
    {
        private readonly RDBContext _context;
        private readonly EmailConfigViewModel _emailConfig;

        public HomeController(RDBContext context, IOptions<EmailConfigViewModel> emailConfig)
        {
            _context = context;
            _emailConfig = emailConfig.Value;
        }



        private async Task SetDefault(ReservationsViewModel vm)
        {
            var date = DateTime.Today;

            if (vm.EventDate == null)
            {
                int start = (int)date.DayOfWeek;
                int target = 0; //Sunday

                if (target <= start)
                    target += 7;

                vm.EventDate = date.AddDays(target - start);
            }

            var query = await _context.Schedules.AsNoTracking()
                .OrderBy(e => e.StartTime)
                .Select(e => new
                {
                    e.Id,
                    e.Description,
                    e.StartTime,
                    e.EndTime,
                    e.Capacity,
                    ReservedSeats = e.Reservations.Where(f => f.EventDate == vm.EventDate)
                        .SelectMany(f => f.ReservedSeats).Count()
                }).ToListAsync();

            var schedules = query
                .Select(e => new
                {
                    e.Id,
                    Description = string.Format("{0} ({1} - {2})",
                        e.Description, date.Add(e.StartTime).ToString("hh:mm tt"), date.Add(e.EndTime).ToString("hh:mm tt")),
                    SeatsAvailable = e.ReservedSeats >= e.Capacity ? "Full" : e.Capacity - e.ReservedSeats + " seat(s) available",
                    Full = e.ReservedSeats >= e.Capacity
                }).ToList();

            int? firstAvailableScheduleId = schedules.Where(e => !e.Full).Select(e => e.Id).FirstOrDefault();

            ViewBag.Schedules = schedules
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.Description + " • " + e.SeatsAvailable,
                    Selected = e.Id == firstAvailableScheduleId
                }).ToList();

            ViewBag.RemainingSeats = schedules
                .Select(e => new Tuple<string, string>
                    (
                        e.Description,
                        e.SeatsAvailable
                    )).ToList();
        }

        public async Task<IActionResult> Index()
        {
            var companionNames = new string[5];

            var vm = new ReservationsViewModel
            {
                Companions = companionNames.Select(e => new ReservationCompanionViewModel
                {
                    CompanionName = e
                }).ToList()
            };

            await SetDefault(vm);
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(ReservationsViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                await SetDefault(vm);
                vm.CustomErrorMessage = "An unexpected error has occurred.";
                return View(vm);
            }

            var selectedCompanions = vm.Companions.Where(e => e.CompanionName != null).ToList();

            var maxCapacity = await _context.Schedules.Where(e => e.Id == vm.ScheduleId)
                .Select(e => e.Capacity).SingleOrDefaultAsync();

            var query = await _context.Reservations
                .Where(e => e.EventDate == vm.EventDate && e.ScheduleId == vm.ScheduleId)
                .Select(e => e.ReservedSeats.Count)
                .ToListAsync();

            var reservedSeatCount = query.Sum();

            var currentCapacity = selectedCompanions.Count + 1 + reservedSeatCount;

            if (currentCapacity > maxCapacity)
            {
                await SetDefault(vm);
                vm.CustomErrorMessage = "Not enough available seats.";
                return View(vm);
            }

            if (selectedCompanions.Any(e => e.Age == null))
            {
                await SetDefault(vm);
                vm.CustomErrorMessage = "The Companion Age field is required.";
                return View(vm);
            }

            var newGuid = Guid.NewGuid();

            try
            {
                var date = DateTime.Now;
                var add = new Reservation
                {
                    Id = newGuid,
                    EventDate = vm.EventDate ?? date,
                    ScheduleId = vm.ScheduleId ?? 0,
                    FirstName = vm.FirstName,
                    LastName = vm.LastName,
                    Age = vm.Age ?? 0,
                    Address = vm.Address,
                    ContactNo = vm.ContactNo,
                    EmailAddr = vm.Email,
                    CreatedDate = date
                };

                await _context.Reservations.AddAsync(add);

                var reservedSeats = new List<ReservedSeat>
                {
                    new ReservedSeat { ReservationId = newGuid, SeatNo = (short)(reservedSeatCount + 1) }
                };

                var companions = new List<Companion>();

                for (var i = 0; selectedCompanions.Count > i; i++)
                {
                    companions.Add(new Companion
                    {
                        ReservationId = newGuid,
                        Name = vm.Companions[i].CompanionName,
                        Age = vm.Companions[i].Age ?? 0
                    });

                    reservedSeats.Add(new ReservedSeat
                    {
                        ReservationId = newGuid,
                        SeatNo = (short)(reservedSeatCount + 2 + i)
                    });
                }

                if (companions.Count > 0)
                    await _context.Companions.AddRangeAsync(companions);

                if (reservedSeats.Count > 0)
                    await _context.ReservedSeats.AddRangeAsync(reservedSeats);

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                await SetDefault(vm);
                vm.CustomErrorMessage = e.Message;
                return View(vm);
            }

            return RedirectToAction(nameof(Success), new { id = newGuid.ToString() });
        }

        private async Task SendEmail(string email, string guid, string successMessage, string firstName, string lastName)
        {
            using var message = new MailMessage();
            message.To.Add(new MailAddress(email, string.Format("To {0} {1}", firstName, lastName)));
            message.From = new MailAddress(_emailConfig.UserName, "From CCF Silang Worship Service");
            message.Subject = "CCF Silang Worship Service Registration";
            message.Body = successMessage + " Ticket ID: " + guid;
            message.IsBodyHtml = true;

            using var client = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(_emailConfig.UserName, 
                    System.Text.Encoding.UTF8.DecodeBase64(_emailConfig.Password)),
                EnableSsl = true
            };
            
            await client.SendMailAsync(message);
        }

        public async Task<IActionResult> Success(Guid id)
        {
            var vm = new ReservationSuccessViewModel();

            using (MemoryStream ms = new MemoryStream())
            {
                var qrGenerator = new QRCodeGenerator();
                var qrCodeData = qrGenerator.CreateQrCode("Ticket ID: " + id.ToString(), QRCodeGenerator.ECCLevel.Q);
                var qrCode = new QRCode(qrCodeData);

                using var bitMap = qrCode.GetGraphic(20);
                bitMap.Save(ms, ImageFormat.Png);

                vm.QRCode = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());
            }

            var reservation = await _context.Reservations
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    Seats = e.ReservedSeats.Select(f => f.SeatNo).OrderBy(f => f).ToList(),
                    e.EventDate,
                    e.Schedule.StartTime,
                    e.Schedule.EndTime,
                    e.EmailAddr,
                    e.FirstName,
                    e.LastName
                })
                .SingleOrDefaultAsync();



            var date = DateTime.Today;

            vm.SuccessMessage = string.Format("You have successfully reserved seat # {0} on {1} {2} - {3}.",
                reservation.Seats.Count > 1 ? reservation.Seats.Min() + "-" + reservation.Seats.Max() : reservation.Seats.First().ToString(),
                reservation.EventDate.ToString("MMMM dd, yyyy"),
                date.Add(reservation.StartTime).ToString("hh:mm tt"),
                date.Add(reservation.EndTime).ToString("hh:mm tt"));

            if (reservation.EmailAddr != null)
            {
                await SendEmail(reservation.EmailAddr, id.ToString(), vm.SuccessMessage, reservation.FirstName, reservation.LastName);
                vm.SuccessMessage += " A confirmation message has been sent to your email.";
            }

            return View(vm);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
