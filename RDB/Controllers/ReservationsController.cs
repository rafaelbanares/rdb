﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RDB.Models;
using RDB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDB.Controllers
{
    public class ReservationsController : Controller
    {
        private readonly RDBContext _context;

        public ReservationsController(RDBContext context)
        {
            _context = context;
        }

        private async Task SetDefault(ReservationListViewModel vm)
        {
            var date = DateTime.Today;

            if (vm.EventDate == null)
            {
                int start = (int)date.DayOfWeek;
                int target = 0; //Sunday

                if (target <= start)
                    target += 7;

                vm.EventDate = date.AddDays(target - start);
            }

            ViewBag.Schedules = await _context.Schedules.AsNoTracking()
                .OrderBy(e => e.StartTime)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = string.Format("{0} ({1} - {2})",
                        e.Description, date.Add(e.StartTime).ToString("hh:mm tt"), date.Add(e.EndTime).ToString("hh:mm tt"))
                }).ToListAsync();
        }

        public async Task<IActionResult> Index()
        {
            var vm = new ReservationListViewModel();
            await SetDefault(vm);

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(ReservationListViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                await SetDefault(vm);
                return View(vm);
            }

            try
            {
                vm.ReservationTable = await _context.Reservations.AsNoTracking()
                    .Where(e => e.EventDate == vm.EventDate && e.ScheduleId == vm.ScheduleId)
                    .Select(e => new ReservationRowViewModel
                    {
                        FullName = e.FirstName + " " + e.LastName,
                        CompanionNames = e.Companions.Select(f => f.Name).ToList(),
                        SeatNumbers = (e.Companions.Count == 0 ? e.ReservedSeats.First().SeatNo.ToString() : 
                            e.ReservedSeats.Min(f => f.SeatNo) + "-" + e.ReservedSeats.Max(f => f.SeatNo))
                    }).OrderBy(e => e.SeatNumbers).ToListAsync();
            }
            catch (Exception)
            {
                throw;
            }

            await SetDefault(vm);
            return View(vm);
        }
    }
}
